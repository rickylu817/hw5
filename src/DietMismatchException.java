

/**
 * The DietMismatchException is a custom exception that is thrown when the OrganismNode
 * is being added a prey OrganismNode that does not match with its dietary preferences.
 * Meaning a herbivore is being fed with meats or carnivore being fed with vegetables.
 */
public class DietMismatchException extends Exception
{
    public DietMismatchException(String msg)
    {
        super(msg);
    }
}