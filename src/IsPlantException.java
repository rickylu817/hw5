

/**
 * IsPlantException is a custom exception that is thrown when the OrganismNode is a
 * plant and the user is attempting to add a prey to the plant OrganismNode, but
 * because it is a plant it cannot have any prey
 */
public class IsPlantException extends Exception
{
    public IsPlantException(String msg)
    {
        super(msg);
    }
}