

/**
 * PositionNotAvailableException is a custom exception that is thrown when the
 * OrganismNode is being added a prey OrganismNode when it have the maximum of
 * three preys already to signal the user that the OrganismNode is full.
 */
public class PositionNotAvailableException extends Exception
{
    public PositionNotAvailableException(String msg)
    {
        super(msg);
    }
}